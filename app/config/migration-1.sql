ALTER TABLE ts_booking_bookings_slots
    ADD COLUMN `repeatInterval` VARCHAR(60);
ALTER TABLE ts_booking_bookings_slots
    ADD COLUMN `repeatUntil` date;

INSERT INTO `ts_booking_fields`
VALUES (NULL, 'booking_slot_repeat_until', 'backend', 'Bookings / Slot repeat until', 'script', '2019-04-08 03:00:00');

SELECT LAST_INSERT_ID() INTO @repeatUntil;

INSERT INTO `ts_booking_fields`
VALUES (NULL, 'booking_slot_repeat_interval', 'backend', 'Bookings / Slot repeat interval', 'script',
        '2019-04-08 03:00:00');

SELECT LAST_INSERT_ID() INTO @repeatInterval;

INSERT INTO `ts_booking_multi_lang`
VALUES (NULL, @repeats, 'pjField', 1, 'title', 'Repeats', 'script'),
       (NULL, @repeatUntil, 'pjField', 1, 'title', 'Repeat Until', 'script'),
       (NULL, @repeatInterval, 'pjField', 1, 'title', 'Repeat Interval', 'script');